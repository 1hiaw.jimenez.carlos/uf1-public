### Ordre de les classes

1. Introducció al shebang. El comprimit ja conté informació sobre com fer aquest tema.
  Pràctic: fent-lo amb els alumnes.
2. Diferents maneres d'executar el shebang.
  Pràctic: fent-lo amb els alumnes, amb algun exercici.
3. Arguments passats per la línia d'ordres.
  El fan els alumnes _amb una petita empenta_.
