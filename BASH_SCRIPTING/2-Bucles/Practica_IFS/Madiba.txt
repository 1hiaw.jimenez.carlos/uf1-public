'In real life we deal, not with gods,
but with ordinary humans like ourselves:
men and women who are full of contradictions,
who are stable and fickle,
strong and weak,
famous and infamous.'
Nelson Mandela 
