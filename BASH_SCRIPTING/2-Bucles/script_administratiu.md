### SCRIPT ADMINISTRATIU

*L'objectiu d'aquesta pràctica és fer un script administratiu molt complet que
faciliti una de les tasques més habituals d'un administrador de sistemes:
vetllar per un consum responsable de l'espai de d'emmagatzematge en xarxa.
Abans però necessitem treballar certs conceptes.*

##### Exercici 1

Com trobo els fitxers més grans d'una certa quantitat? I d'una certa
antigüitat?

##### Exercici 2

Anem a configurar l'enviament de correu electrònic a la nostra intranet (també
podríem configurar un compte de correu d'un proveedor extern: gmail, yahoo...
però aquest no és l'objectiu d'aquest exercici)

	
Com a root instal·larem `sendmail` si no està instal·lat, utilitzarem
`sendmail.sendmail` com a *mta* i inicialitzarem el servei de `sendmail`:

```
[root@localhost ~]# yum install sendmail -y
[root@localhost ~]# alternatives --config mta
[root@localhost ~]# systemctl start sendmail.service
```

Quina és la variable que conté el directori on s'emmagatzema el correu?

Quin és aquest directori?

Hi ha algun altre directori on també es desi el correu? Com ho expliques?

A quin fitxer de configuració es troba aquesta variable? 

A quin fitxer de configuració es troba la variable CREATE_MAIL_SPOOL (yes/no) que m'informa de si es crearà la bústia de correu o no?

Com puc, per exemple, enviar amb una sola ordre un email amb un contingut que ja tinc a un fitxer? (jugueu amb canonades, és a dir amb *pipes*)

##### Exercici 3

**Ordre `at`**.

Volem executar una ordre d'aqui a 1 minut. Com ho fem? L'ordre `at` rep (una
altra) ordre a executar *des de l'entrada estàndard*. Ajudant-vos del `man`,
executeu l'ordre `touch f1` d'aquí un minut. Feu el mateix amb l'ordre `echo
"hola"`. Si el resultat us sorpren busqueu ajuda al `man`. Un cop resolt el
misteri anterior, sabríeu mostrar el missatge a la terminal a on us trobeu?
(*Hint*: `tty` command)

---

##### SCRIPT

Fer un *shell script* acomplint les següents condicions:

1.  Llisti tots els usuaris que tinguin arxius més grans de X Mbytes i més antics
de Y dies.

2. Posteriorment generi un arxiu que contingui el nom de tots els fitxers amb el path complet i enviar un correu a cada usuari que compleixi aquesta condició amb un avís que els informi de que han de salvar (backup) aquests arxius perquè seran eliminats del sistema.

3. Aixi cada usuari rebra en el correu mencionat els noms d'aquests arxius.

4. A l'script s'utilitzarà l'ordre `at` per executar la comanda que esborri tots els arxius del fitxer generat 48 hores després d'haver enviat el correu. Treballarem en local, i l'script se suposa que l'executa l'administrador del sistema, o sigui *root*. És recomanable que creeu 4 o 5 usuaris i jugueu amb diferents mides i antiguitats de fitxers.

**RECORDEU FER PRIMER UNA CAPÇALERA, UNA BONA DESCRIPCIÓ I COMENTARIS**

