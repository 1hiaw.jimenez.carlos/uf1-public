### Exercicis d'introducció a l'estructura condicional __if__

Abans de fer els següents exercicis estudieu l'ordre `read`

El seu us més habitual és:
```
read nom_de_variable
```

+ Esbrineu com funciona executant alguns exemples.
+ Com es recupera el contingut de la línia?
+ Si no utilitzeu cap variable, la línia que llegeix l'ordre _read_
s'emmagatzema en algun lloc o es perd? (hauríeu de cercar ajuda)
+ Quin _errorlevel_ em retorna l'execució de _read_ si poso una línia _normal_?
+ Quin _errorlevel_ em retorna _read_ si no escric res i directament executo l'intro?
+ Recordeu que vam explicar que hi ha un caràcter que representa el final d'arxiu. Feu la prova
introduint aquest caràcter (sense l'intro). Quin errorlevel obteniu?


#### Exercici a)
Feu un script que llegeixi una línia introduida per l'usuari i que mostri per pantalla el missatge:
```
linia llegida: bla bla bla ...
```
i, en canvi, si s'ha introduit el senyal de final d'arxiu mostrarà:
```
no s'ha introduit cap línia
```

#### Exercici b)
Feu que l'script sigui més amable: volem que es mostri un missatge de manera que l'usuari sàpiga que ha de fer quan executa l'script.

Per exemple:
```
[leviatan@pc666 hell]$ ./llegeix_linia.sh
escriu el que vulguis i tot seguit prem intro
```

(Vigila dintre de l'script quina ordre escrius primer)

#### Exercici c)
Ara volem diferenciar entre els 3 casos possibles que hem comentat abans:
- línia buida
- línia normal
- senyal de final de fitxer

Per tant l'script em mostrarà:
```
linia llegida: buida
```
```
linia llegida: bla bla bla
```
```
no s'ha introduit cap línia
```
respectivament

Hints:

* man test per saber si una cadena és buida.
* es pot posar un _if_ dintre d'un _if_?

###### Nota

Perquè el nostre fitxer `vim` identi de manera automàtica, hem d'escriure en mode ordre:

```
:set autoindent
```

recordem a més que si volem que el tabulador sigui de 4 espais en blanc hauríem d'afegir:

```
:set tabstop=4
```

O per afegir números (per cada línia):

```
:set nu
```

i si volem que en comptes de temporal, això sigui permanent, ho afegim al
fitxer ocult .vimrc (sense el caràcter `:`):

```
 ~/.vimrc
```

_**Recomanació molt important: mai, mai utilitzeu una ordre per primera vegada
dintre d'un script, abans jugueu amb ella des de la consola fins que enteneu bé
com funciona.**_


