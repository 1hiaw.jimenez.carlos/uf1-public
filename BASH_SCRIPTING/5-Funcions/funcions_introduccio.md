### Introducció a les funcions en Bash Scripting

##### Exemple introductori

Llegim el [següent script](suma_resta_funcions.sh), tot entenent-lo.

Executem després algun parell d'exemples:

```
./suma_resta_funcions.sh 50 + 34
./suma_resta_funcions.sh 70 - 2
```

Plantegem-nos que passa si des de la consola intentem executar:

```
./suma_resta_funcions.sh 150 + 150 
./suma_resta_funcions.sh 150 - 151 
```

Que ha passat?

Hi ha diferents maneres de solucionar això, veiem un parell:

1. Una primera és utilitzant una variable global.
2. Una altre és fent que la funció utilitzi un `echo` per mostrar el valor de retorn ...

Anem a practicar-ho: modifiqueu l'script [suma_resta_funcions.sh](suma_resta_funcions.sh) aconseguint 2 versions de l'script a partir de les dues solucions anteriors.

##### Exercici

Feu una altra versió de l'*script* a partir de la *Solució 2*. Heu d'afegir una funció de nom *show_result* que rebrà el resultat i mostrarà la cadena "El resultat és: resultat".

##### Troubleshooting

Oblidar-se un espai quan declarem una funció ens pot donar un error de sintaxi difícil de detectar:

```
function nom_de_funcio{
```

en comptes de:

```
function nom_de_funcio {
```

##### Observacions

*El tractament de funcions de bash scripting es [pot complicar tot el que vulguem i imitar comportaments semblants amb altres llenguatges](http://tldp.org/LDP/abs/html/complexfunct.html), però si necessitem de certes tècniques molt avançades potser és que hem escollit el llenguatge que no toca :)*

