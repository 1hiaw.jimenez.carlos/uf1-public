### Exercicis funcions de bash scripting

##### Exercici 1

Feu una funció que mostri els caràcters d'una cadena separats per línies.

##### Exercici 2

Feu una funció que rebi una cadena i un caràcter i que digui si el caràcter està contingut a la cadena.

##### Exercici 3

Feu una funció (i per tant un script per provar que és correcte) que rebi un array d'enters i els ordeni utilitzant el mètode de la bombolla(bubble-sort), per exemple de forma descendent.

##### Exercici 4

Feu el codi de la funció *swap* que intercanvia dues variables. Es pot fer utilitzant variables globals, però sobretot volem alguna solcuió que no faci servir aquest tipus de variable.

El codi seria de tipus:

```
function swap {
	to_do
	to_do
}
a=1
b=2
echo «els valors d'a i b són: $a $b»	# Aqui es mostrarà 1 i 2
swap a b								# Alerta, no passem *$a* sinó *a*
echo «els valors d'a i b intercanviats són: $a $b»    # Aqui es mostrarà 2 i 1
```

##### Exercici 5

No treballarem més amb funcions que requereixin rebre una estructura de dades,
   canviar el seu valor i retornar-la. Però resolem abans l'exercici 3,
   bubblesort, utilitzant la tècnica que hem vist de la **referència
   indirecta**.

##### Exercici 6

Feu l'exercici 39 del curset de bash scripting:

Dissenyar un script anomenat *opf* que permeti copiar (-c), moure (-m) i
esborrar (-d) fitxers. L'script ha de comprovar que la sintaxi utilitzada és la
correcta.

