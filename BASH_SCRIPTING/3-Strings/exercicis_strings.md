### Exercicis de cadenes amb Bash Scripting

###### Exercici 1

Com puc calcular al bash la longitud d'una cadena?

###### Exercici 2

Quina instrucció mostrarà la subcadena de $string que comença a la posició $position?

###### Exercici 3

Quina instrucció mostrarà la subcadena de $string que comença a la posició $position i té com a longitud $len caràcters?

###### Exercici 4

Quina instrucció mostrarà la subcadena de $string formada pels darrers $len caràcters?

###### Exercici 5

Quina instrucció substituirà a la cadena $string la subcadena $substr1 per la subcadena $substr2? i totes les ocurrències que hi hagi?

