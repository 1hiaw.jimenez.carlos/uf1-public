## Exercicis arrays

#### Arrais indexats (dimensió 1)

Escrivim a la consola les següents expressions i hem d'interpretar el seu significat: 

##### Exercici 1

```
numeros=( 1 2 3 )
```

```
echo ${numeros}
1
```

```
echo ${numeros[0]}
1
```

```
echo ${numeros[1]}
2
```

```
echo ${#numeros}
1
```

```
echo ${numeros[@]}
1 2 3
```

```
echo ${numeros[*]}
1 2 3
```

Amb el que hem vist, com es faria per mostrar la longitud de l'array?

##### Exercici 2

Hem vist a l'exercici anterior com es declara un arrai de forma implícita.

Una manera de fer-ho de manera explícita és:

```
declare -a nombres
```

Si ens preguntem sobre la conveniència de declarar l'arrai d'una manera o una altra [aquest enllaç](http://tldp.org/LDP/abs/html/arrays.html#ARRAYASSIGN) ens pot ajudar a decidir:

> *Adding a superfluous `declare -a` statement to an array declaration may speed up execution of subsequent operations on the array.*


Si volem utilitzar l'operador {1..3} hem de vigilar com el fem servir:

```
nombres={1..3}  # incorrecte
```

```
nombres=({1..3})  # correcte
```

No és necessari que els elements de l'array siguin consecutius:

```
valors[2]=20
valors[5]=50
echo ${valors[@]}
```


o fins i tot:

```
valors=( [7]=70 [9]=90 )
echo ${valors[@]}
70 90
```

##### Exercici 3

L'operació anterior ens serveix per comprovar que l'assignació torna a inicialitzar el array.

Es poden afegir elements a un array ja existent?

##### Exercici 4

Tenim l'array de longitud 5:

```
values=( 3 44 555 7 8 )
```

i volem aconseguir que la variable *new_values* sigui un arrai sense el darrer element:

```
echo ${new_values[@]}
3 44 555 7

```

com ho puc aconseguir? 

*Hint:* apliqueu *slicing*

##### Exercici 5

Volem mostrar tots els elements de l'arrai anterior, tot i que tingui *forats*, des de l'índex 0 fins a l'últim.

Haurem d'emmagatzemmar certes dades primer.

##### Exercici 6

Feu l'algoritme de la bombolla (*bubble-sort*) en bash scripting utilitzant arrais.

Podeu fer servir [notació *C-like*](http://tldp.org/LDP/abs/html/dblparens.html)

També pot ser útil fer servir les expressions:

```
{a..z}, {1..7}, {00..99}, {1..20..2}
```

#### Arrais associatius

Els arrais associatius només es poden declarar de forma explícita amb la instrucció `declare -A`:

```
declare -A notes
notes[Sistemes]=7
notes[Programacio]=6
notes['Base de Dades']=9
notes[XML]=8
```

Mirem els valors:

```
echo ${notes[@]}
9 7 6 8
```

Els indexos són:

```
echo ${!notes[@]}
Base de Dades Sistemes Programacio XML
```

Un arrai associatiu (també anomenat hash ) és una col·lecció que relaciona claus amb valors i no podem garantir que hi hagi cap ordre. De fet és possible que no s'hagi mantingut l'ordre en el qual hem fet les assignacions.

Això últim es podria veure amb l'ordre `set` que ens mostra totes les variables:

```
set| grep notes
notes=(["Base de Dades"]="9" [Sistemes]="7" [Programacio]="6" [XML]="8" )

```

##### Links

* [Teoria molt completa](http://wiki.bash-hackers.org/syntax/arrays)
* [Tractant *no arrais* com *arrais*](https://www.tldp.org/LDP/abs/html/abs-guide.html#AEN18812)
* [Bash scripting és un llenguatge dèbilment *tipat*](https://www.tldp.org/LDP/abs/html/abs-guide.html#BVUNTYPED)
* [Introducció](http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_10_02.html)

