## Exercicis tema LPI 103.3 Tasques bàsiques d'administració de fitxers

##### Exercici 1.

Acabeu de treballar el listing 4 dels apunts de IBM suposant que teniu un fitxer qualsevol _text1_ que es troba al nostre `$HOME/iawxxxxx` de gandhi. Feu:

```
ls -i text1
```

Després:

```
mv text1 un_directori_de_gandhi/text2
ls -i un_directori_de_gandhi/text2
```

I finalment fer l'operació anterior però desant una còpia del nostre fitxer al pen:

```
mv  un_directori_de_gandhi/text2  un_directori_del_pen_usb/text2
ls -i un_directori_del_pen_usb/text2
```

Compareu els inodes que ens han sortit en els 3 casos i expliqueu aquest comportament?

Per acabar copieu un fitxer prou gran de _public_ a `/tmp`,com per exemple `/home/groups/inf/public/install/IDE/android/android-studio/android-studio-ide-141.2135290-linux.zip` i poseu davant de l'ordre `cp` l'ordre `time`.

Després moveu el mateix fitxer zip de `/tmp` a un subdirectori qualsevol de `/tmp`.
Triga el mateix temps? Perquè?

---

_Utilitzeu l'ordre `find` per aconseguir el que es demana als següents exercicis:_

##### Exercici 2.

Llisteu tots els fitxers del directori actual que han estat modificats a l'últim dia.

##### Exercici 3.

Llisteu tots els fitxers del sistema que són més grans d'1 MB.

##### Exercici 4.

Elimineu tots els fitxers amb extensió "class" de l'estructura de directori que comença al vostre directori personal. Aquest exercici pot ser molt perillós, es recomana fer una còpia del directori a on teniu els fitxers de Java al directori `/tmp` i jugar a `/tmp`.

##### Exercici 5.

Llisteu els inodes de tots els fitxers java del directori actual.

##### Exercici 6.

Llisteu tots els fitxers del sistema de fitxers local (per tant sense incloure els de gandhi ja que pertanyen a un altre sistema de fitxers) que hagin estat modificats a l'últim mes.

##### Exercici 7.

Trobeu tots els fitxers del vostre directori personal que tinguin extensió *java* o *sql*.

##### Exercici 8.

Trobeu els fitxers ocults (regulars, o sigui no directoris) que es troben al nostre directori `$HOME`

##### Exercici 9.

Trobeu els subdirectoris que pengen directament del nostre $HOME (i.e sense incloure els subdirectoris dels subdirectoris)

##### Exercici 10.

Executeu les següents instruccions des del vostre HOME per exemple:

```
[jamoros@heaven ~]$ ls -la
total 520592
drwx--x--x. 24 jamoros inf       4096 Nov 19 11:47 .
drwxr-xr-x. 17 root    root      4096 Oct 24 15:38 ..
-rw-------.  1 jamoros inf      17061 Nov 19 11:32 .bash_history
-rw-------.  1 jamoros inf         18 Sep  4 13:29 .bash_logout
...
```

```
[jamoros@heaven ~]$ find -maxdepth 1 -type d | wc -l
23
```

Veieu alguna relació entre el *24* de la línia del directori `.` a la primera instrucció i el *23* de la 2a instrucció?

##### Exercici 11.

Comenteu quina és la diferència entre les següents ordres:
```
find -name "*.sh" -print -exec cat '{}' \;
find -name "*.sh" -print | xargs cat
```
---

##### Exercici 12.

Creeu un directori anomenat 'pare' que contingui tres arxius anomenats fill1.txt, fill2.txt i fill3.txt amb el text “Hola, soc el fill NUM”, on NUM es el numero de fill de cada fitxer. Comprimiu cadascú d'aquests fitxers amb gzip i poseu-los en un directori anomenat “paregz”.

1. Coneixeu alguna família de comandes que permeti visualitzar els continguts dels fitxers comprimits sense tenir que descomprimir-los?

2. Comprimiu el directori *pare* i tot el seu contingut mitjançant `gzip` amb el grau màxim de compressió. Feu el mateix utilitzant `bzip2`. Compareu els resultats i comenteu-los. Creieu que les conclusions que n'heu extret d'aquesta prova són extensibles a la compressió d'altres fitxers? per què?

3. Descomprimiu els dos últims fitxers que heu creat (*.gz* i *.bz2*) en directoris diferents, i comproveu de forma automàtica que els continguts siguin idèntics.

##### Exercici 13.

Contesteu les següents qüestions sobre la comanda `file` i els magic numbers:

* On es configuren i defineixen tots els magic numbers usats per la comanda `file`?

* Quina funció fa la comanda `hexdump`?

* Creeu un arxiu *tar* (podeu crear un conjunt d'arxius qualsevol o be copiar alguns arxius existents) i a continuació comprimiu-lo amb gzip generant per exemple l'arxiu arxiu.tar.gz. Feu la mateixa operació però comprimint amb bzip2 obtenint per exemple el fitxer arxiu2.tar.bz2. (es poden fer servir els arxius comprimits de la pregunta anterior)

* Mitjançant la comanda hexdump comproveu que cada tipus de fitxer (gzip i bzip2) es corresponen amb l'especificat a la definició del magic number corresponent al seu format (feu man hexdump per veure els diferents tipus de sortida de la comanda). Comenteu el que heu obtingut/vist.

****************
---

Diferències entre mtime, ctime, atime (paràmetres de l'ordre find):

* http://www.geekride.com/inode-structure-ctime-mtime-atime/

* http://www.linuxtotal.com.mx/index.php?cont=info__tips_022


[Curiositats amb comprimits recursius](https://research.swtch.com/zip)

