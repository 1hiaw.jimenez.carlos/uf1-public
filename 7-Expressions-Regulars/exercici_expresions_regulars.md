## Exercicis expressions regulars (I) 

### Exercici 1

Completeu les següents _definicions_ relacionades amb les expressions regulars:

```
^	que significa ...
$	que significa ...
.	que significa ...
*	que significa ...
+	que significa ...
?	que significa ...
[a-z]   que significa ...
[0-3] 	que significa ...
[376a-dm] que significa ...
```

Jugueu a la consola amb instruccions del tipus:

```
echo "a" | grep '^.$'    # Aqui grep busca una línia que contingui ...
echo "aa"| grep '^.$'
```

per trobar quina expressió representa una cadena formada *exclusivament* per caràcters numèrics

### Exercici 2

Un cop resolt l'anterior exercici, feu un script que rebi un argument numèric,
controlant que hi hagi un únic argument, en cas contrari sortirem de l'script
com sempre. Volem que també controli que el que li passem sigui un número
enter. En cas contrari sortim de l'script adequadament. Finalment mostrarem una
cadena indicant si és parell o senar.

### Exercici 3

Feu el mateix que abans però ara el número serà real. Penseu que el número
podrà tenir signe positiu/negatiu o no tenir signe, tanmateix recordeu que els
reals poden tenir un separador decimal `.`. En aquest exercici de números
reals no mostrarem si és parell o senar.

### Exercici 4

Trobeu tots els *usuaris* amb un numero id més gran que 990 i més petit que
1000. O sigui *99x*.

### Exercici 5

Volem canviar de l'usuari amb uid 1000 (si no teniu aquest id feu-lo amb un
altre) el camp GECOS posant únicament "usuari local". Primer obteniu el
resultat per pantalla i despres ja pensareu com canviar-ho directament al
fitxer. Per cert si torneu a executar l'ordre amb la qual heu solucionat el
problema anterior segueix funcionant bé.

No utilitzeu el fitxer ```/etc/passwd```, feu una còpia.


### Exercici 6

Com passarieu una certa cadena de majúscules a minúscules o a l'inrevés?

___
#### Nota

Per als exercicis que necessitin `/etc/passwd`, el format de cada línia del
fitxer és:

```
username:password:UID:GID:GECOS:homedir:shell
```

(_GECOS_ és un camp que es manté per raons històriques, normalment s'aprofita
per donar informació addicional de l'usuari)


