# Instal·lació de Fedora 32 a portàtil

Repositori amb els guions de la instal·lació de Fedora 32 feta a diferents portàtils, conservant o no el sistem a operatiu preexistent. O sigui que el portàtil tindrà arrancada dual o no.

El fitxer markdown tindrà com a títol el model de portàtil utilitzat, però el nom del fitxer markdown només contindrà caràcters de l'alfabet anglès.

Els guions es dividiran en diferents seccions: 

## Especificacions (RAM, disc dur, arquitectura, targeta gràfica, targeta wifi, touchpad ...).

## Tipus d'arrencada (dual o no).

## Tipus d'instal·lació: BIOS legacy o UEFI.

## Links de referència per fer la instal·lació.

## Links de referència de problemes d'algun element del maquinari del portàtil amb Fedora 32.

## Canvis a la BIOS en funció de la instal·lació escollida (UEFI, legacy, amb secure boot o sense ).

## Tipus de taula de particions escollida (DOS/GPT).

## Preparació particions.

## Instal·lació.

## Post-instal·lació.

## Problemes (troubleshooting) i resolucions.

> Farem servir les característiques que ens proporciona el llenguatge markdown (links, imatges, llistes, títols, èmfasi, més èmfasi ...)
