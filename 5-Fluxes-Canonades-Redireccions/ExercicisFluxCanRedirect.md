### Exercicis fluxos, canonades i redireccions

##### Exercici 1

Anem a revisar certs conceptes que ja hauríem de dominar.  

###### a) 

Mostreu tots els fitxers dels directoris de `/lost+found` o `/root`, de manera
que ens sorti algun error perquè no tinguem permisos.

###### b)

Xequejeu amb alguna ordre si hi hagut alguna errada

###### c)

L'error estàndard es pot redireccionar amb `2>`.  D'altra banda, els missatges
que no volem que es mostrin els podem enviar a `/dev/null` (Ull viu, no estem
dient que els missatges d'error no serveixin!).

###### d)

Comproveu ara si ha hagut error.

###### e)

Ara volem perdre de vista els missatges d'error però la sortida estàndard la
volem enviar al fitxer _sortida.txt_.

###### f)

Ara volem fer el mateix d'abans però, a més a més, que el missatge de la
sortida estàndard també surti per pantalla.

##### g)

El mateix d'abans però emmagatzemant els missatges d'error a un altre fitxer
en comptes de perdre'ls?

##### h)

Volem cercar certa informació només de la sortida estàndard de l'error, la
resta no m'interessa gens. Ho fem de la següent manera:

```
ls /root /var /lost+found > /dev/null 2> sortida.txt
cat sortida.txt | grep 'patró a cercar'
```

És possible fer-ho sense el fitxer temporal sortida.txt?

---

##### Exercici 2.

Al listing 1 del [tutorial d'IBM de fluxes, canonades i
redireccions](https://developer.ibm.com/tutorials/l-lpic1-103-4/#listing1) hi
ha una instrucció que no és trivial:

```
echo "This is a sentence. " !#:* !#:1->text3
```

Intenta esbrinar què fa. Fes-ho *a poc a poc*, agafant només una part i afegint
després la resta.

Alerta: agafa el guionet d'aquest enunciat, NO el del listing que no és el
mateix caràcter!

Si et rendeixes, demana una pista ;)

##### Exercici 3.

Ja sabem que tot i que semblants, hi ha diferències substancials quan
executem les següents instruccions:

```
find ....   -exec ordre '{}'  \;
find ....  | xargs ordre
```

- A la primera instrucció quan trobem la primera ocurrència li passo
aquesta a l'ordre com que té d'argument `-exec` perquè s'executi, després
cerco la següent ocurrència i quan la trobo li torno a passar a l'ordre.
- En canvi amb `xargs`, trobo totes les ocurrències de cop i després li
passo a l'ordre.

Si pensem en eficiència està clar que la 2a opció és millor que la 1a. De fet
seria semblant a preguntar-nos quina de les següents ordres és més ràpida:

```
ls /dir1; ls /dir2; ls /dir3; ls /dir4; ls /dir5; ls /dir6; ls /dir7
ls /dir1  /dir2  /dir3  /dir4  /dir5  /dir6  /dir7
```

Intuïtivament sembla clar que és més eficient cridar a una ordre una vegada
i passar-li diferents paràmetres en comptes de cridar a la mateixa ordre
moltes vegades.  Comproveu això últim posant l'ordre time davant de cada
ordre per veure quan de temps triga (quan l'ordre és _composta_ podeu posar
unes claus amb `;` al final `{ ...; }`).

_xargs_ te d'altres problemes amb el tractament de cadenes com
se us explica als apunts d'IBM.

###### a)

Si busqueu una mica, al _man_ per exemple, trobareu que per a versions modernes
de `find` podem aconseguir que `-exec` actuï com al `xargs`, és a dir que
primer es trobin totes les ocurrències i que després se li passin a l'ordre per
executar una única vegada. Trobeu com s'ha de fer això.

###### b)

Als apunts de LPI d'IBM se us explica que si volem utilitzar la sortida d'una
ordre o el contingut d'un arxiu com a argument d'una altre ordre les canonades
(pipes) no funcionen i que disposem d'almenys 3 mètodes.  Practiquem una mica.
Expressa la següent ordre, que utilitza substitució de comandes, amb `find ...
-exec` i amb `find | xargs`:

```
more $(ls /etc/*.conf)
```

---

##### Exercici 4.

Suposem que el fitxer file1 conté únicament la línia:

```
Telemadrid, espejo de lo que somos
```

i que el nostre objectiu és aconseguir que canviï per:

```
Telemadrid, espe jode lo que somos
```

Això ho podríem solucionar amb els filtres, jugant amb `sed`, `tr` i
d'altres ordres, però ho volem fer a més baix nivell, jugant amb les
redireccions. Primer resoldrem les següents qüestions:

###### a)

Que fa l'ordre `exec` quan no li passem cap ordre? (`man exec`)

###### b)

Puc utilitzar números superiors a 2 com a file descriptor per qualsevol fitxer?
Com es fa?

###### c)

Puc enviar la sortida estàndard o l'entrada estàndard d'una ordre a un
fitxer mitjançant el file descriptor? Com es fa?

###### d)

Com puc obrir un fitxer, _file1_ per exemple, per llegir i escriure alhora i
assignar-li el _file descriptor 3_ per exemple?

###### e)

Com puc utilitzar `read` perquè llegeixi 16 caràcters, o el que és el mateix,
el punter es quedi apuntant al caràcter número 17?

###### f)

Com puc fer el que em demanen però suposant que llegeixo d'un fitxer el file
descriptor del qual és 3?

###### g)

Quina seria llavors la solució final que resol el problema?

##### Exercici 5.

Hem rebut un vídeo d'un usuari anònim on es veu com treballa amb el bash. Ens
repta a que esbrinem quina ordre va utilitzar inicialment per aconseguir el que
es veu al vídeo `missatges_perduts.ogv`

##### Exercici 6.

Volem tornar a fer l'script _header.sh_ però en comptes de fer servir els
`echo` per enviar línies al nou fitxer script farem servir els `here-document`.

###### a)

Creeu un script molt senzill, que enviï a un cert fitxer, per exemple file1,
una certa cadena, per exemple *hola que tal* mitjançant _here-document_.

###### b)

Modifiqueu l'script _header.sh_, de manera que faci el mateix però utilitzant
els _here-document_. Agafeu la darrera versió que teniu al moodle i canvieu
només el que sigui necessari.

Links:
+ <http://en.wikipedia.org/wiki/Xargs>
+ <http://www.tldp.org/LDP/abs/html/io-redirection.html>

![/dev/null](dev_null.jpeg)
